import {useEffect, useState, useCallback} from 'react';
import {useParams} from 'react-router';
import {getServiceDetalhe} from '../services/atividade.service'
import FormularioInscricao from '../components/formularioInscricao'
import TabelaInscritos from '../components/tabelaInscritos'
import styled from 'styled-components';
import {Jumbotron, Button, Container, Row} from 'reactstrap';

const Detalhes = (props) => {

    const { id } = useParams();
    const { history } = props // pega o histórico do component, para em caso de erro usar a rota de onde estava para voltar.(dentro da callback)
    
    const [loading, setLoading] = useState(false); // quando iniciar o ciclo de vida de renderização o loading se transformará em true e quando acabar voltará para false.
    const [detalhe, setDetalhe] = useState({});//começa com objeto vazio e depois pegará as informações da atividade especifica, pelo id (descricao, faixa_etária e inscricoes)
    const [update, setUpdate] = useState(false); //o ciclo de vida vai escutar a funcao de pegar os detalhes da atividades especifica, mas tambem vai escutar a funcao update, quando ela for setada em true (apos inscricao ou delete de uma inscricao), permitindo o ciclo de renderização reiniciar.
    const [isSub, setSub] = useState(false); //toggle entre inscricao na atividade e mostrar a lista de inscritos.

    const getDetalhes = useCallback(async () => {
        try {
            setLoading(true)//inicio do ciclo de vida
            const res = await getServiceDetalhe(id); //veja na rota qual é o id da atividade especifica em que nos encontramos.
            setDetalhe(res.data);//guarde dentro da const detalhe os dados trazidos pela response, sem o header e outros detalhes.
            setLoading(false)//fim do ciclo de vida

        } catch (error) {
            console.log('##', error)
            history.push('/?error=404')//condicional: houve erro 404, volte para onde estava.
        }

    }, [id, history]); //acompanhe as alterações de id e history, caso mude, chame a funcao novamente.


    // use Effect é o ciclo de vida que executa antes* de renderizar a pagina.
    useEffect(() => {

        getDetalhes();//quando chama a funcao getDetalhes dentro da useEffect está iniciando de fato o ciclo de vida.
        setUpdate(false);//toda vez que finalizar um ciclo de vida temos que repor o update para false. Ele começa false, mas como a useEffect está acompanhando o update, ela reiniciará um novo ciclo se perceber que houve update.

    }, [getDetalhes, update]);



   
    const jumboDetalheAtividade = (detalhe) => (
        <SJumbotron>
            <Container>
            <Row className="detalhesAtividades">
                <p><strong>Atividade: </strong>{detalhe.descricao}.</p>
                <p><strong>Faixa etária indicada: </strong>{detalhe.faixa_etaria}.</p>               
            </Row>
            <Row>
            <img src={detalhe.imagem} alt={detalhe.descricao}/> 
            </Row>
            </Container>
        </SJumbotron>
        )

   
    const Menu = () => (
            <SContainer className="navbar-expand-md">
                <p className="d-none d-md-block">{isSub ? "Exibir lista de inscrições" : "Aceda ao formulário de inscrições"}</p>
                <Button onClick={() => setSub(!isSub)} color={!isSub ? "primary" : "success"} size="sm">
                    {!isSub ? "Cadastrar" : "Listar inscrições"}
                </Button>
            </SContainer>
    )

    const displayDetails = (detalhe) => (
        <>
            {jumboDetalheAtividade(detalhe)}
            {Menu()}
            {
                // [condicao] ? [true] : [false]
                isSub
                    ? (<FormularioInscricao
                        id={id}
                        update={setUpdate}
                        isForm={setSub} />)
                    : ( 
                        <TabelaInscritos
                        inscritos={detalhe.inscricao}
                        update={setUpdate}
                        />
                       
                    )
            }
        </>
    )

    


    return (
        loading
        ? 'loading'
        : displayDetails(detalhe)
       
    )

}

export default Detalhes;


const SJumbotron = styled(Jumbotron)`
 
    padding: 40px 0;
    margin: 60px 0;
    background-color: #fff38a;
    border-top: 3px solid #DEB887;
    border-bottom: 3px solid #DEB887;
   

    .container {
        display: flex;
        align-items: center;
        justify-content: space-around;
        flex-wrap:wrap-reverse;
        
    }
    
    

    img {
        max-width:520px;
        max-height:350px;
        object-fit: cover;
        border-top: 1px solid #ff66d8ff;
        border-bottom: 1px solid #ff66d8ff;
        border-left: 1px solid #59cd90ff;
        border-right: 1px solid #59cd90ff;

    }
    

    .detalhesAtividades {

        display: flex;
        flex-direction: column;
        font-size: 24px;
        color: #2B3A67;

        strong{

            font-weight: bold;
        }
    }


`
const SContainer = styled(Container)`
    display: flex;
    flex-direction: row;
    margin:40px auto;
    padding:10px;
    border-bottom: 1px dotted black;
    color: #2B3A67;
    
    
 

    p{
        flex:1;
        font-weight: bold;
        margin-top: auto;
        margin-bottom: auto;
        font-size: 20px;
        
        

    }



`