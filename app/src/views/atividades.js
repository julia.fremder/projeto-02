import React, { useState, useEffect, useCallback } from 'react';
// import http from '../config/http'
import { getAllServiceAtividades } from '../services/atividade.service';
import Banner from '../components/banner';
import { Col, Container, Row, Table } from 'reactstrap'
import ItemAtividade from '../components/atividade/item_atividade';
import styled from 'styled-components';


const Atividades = () => {

    const [atividades, setAtividades] = useState([]);
    const [loading, setLoading] = useState(false);


    const getAllAtividades = useCallback(() => {
        setLoading(true);
        getAllServiceAtividades()
          .then(res=> {
              setAtividades(res.data);
              setLoading(false)

          })
          .catch(err => {

            alert('Error: '+ err);
            setLoading(false);  
          })
      },[])

    useEffect(() => {
        getAllAtividades();
    }, [getAllAtividades])

    
    const MapAtividades = (atividades) => atividades.map((item, i) => (
      <Col md="4" xl="4" sm="6" xs="12" key={i} className="container-fluid">
        <ItemAtividade item={{...item}}/>    
      </Col>
    ))

    const MapTabelaAtividades = (atividades) => atividades.map((item, i) => {
      
      console.log(item)
      return (
        
                    
                        
                            <tr  key={i} >
                                <td>{item.descricao}</td>
                                <td>{item.faixa_etaria_indicada}</td>
                                <td>R$ 50,00</td>
                              
                            </tr>

              
    )
      })



/* <Link className="link" to={`/atividade/${item.id}`}>
<li key={i}>
  <p className="faixa">Faixa etária indicada: {item.faixa_etaria}.</p>

  <img
    className="imagens"
    src={item.imagem_URL}
    alt="Oficinas infantis"
  />
  
  <p className="inscricaoAtiv">{item.descricao}</p>
</li>
</Link>) */

    return (
      <>
        <Banner />
        <SAtividade>
        <Row className="container">
        {loading ? 'loading' : MapAtividades(atividades)}
        </Row>
        </SAtividade>
        <SContainer>
          <p>Tabela de valores</p>
        <Table striped responsive >
                    <thead className="bg-success">
                        <tr >
                            <th>Atividade</th>
                            <th>Faixa Etária</th>
                            <th>Valor</th>
                        </tr>
                    </thead>
                    <tbody className= "table-success" >
        {loading ? 'loading' : MapTabelaAtividades(atividades)}
        </tbody>
        </Table>
        </SContainer>
      </>
    );

};




export default Atividades;


const SAtividade = styled.div`
    
    background-color: #ffee57;
    border-top: 3px solid #dcbb57;
    border-bottom: 3px solid #dcbb57;
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: center;
    flex:1;
    padding: 40px;
`
const SContainer = styled(Container)`

    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: center;
    padding: 40px;
    color: #2B3A67;

    p{

        width: 100%;
        text-align: left;
        font-weight: bold;

    }

    .table-responsive, table {

      font-size: 14px;

      thead{

        padding:0;
        margin:0;

      }


      hr{

        margin:0;
        color: green;
        padding:0;

      }
     
      tr{

        padding: 5px;

        th, td {

            color: #191970;
            :nth-child(1){text-align: left;}
            :nth-child(2){text-align: center;}
            :nth-child(3){text-align: center;}

        }


      }
      

    }


`