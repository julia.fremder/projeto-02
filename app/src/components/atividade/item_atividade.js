import { Link } from 'react-router-dom';
import {
    Button,
    Card, CardBody, CardImg,
    CardTitle
} from 'reactstrap';
import styled from 'styled-components';

const ItemAtividade = (props) => {
    const { id, descricao, imagem_URL } = props.item;

   

    return (
        <Lista>
            <SCardBody>
                <SCardTitle tag="h5">{descricao}</SCardTitle>
                <Image top width="100%" src={imagem_URL} alt="oficina de colonia de ferias" />
            </SCardBody>
            <Button size="sm"  tag={Link} to={`/atividade/${id}`} color="link">Entrar</Button>
        </Lista>
    )
}

export default ItemAtividade;


const Image = styled(CardImg)`
    width:200px;
    height:150px;
    object-fit: cover;
    border-top: 1px solid #ff66d8ff;
    border-bottom: 1px solid #ff66d8ff;
    border-left: 1px solid #59cd90ff;
    border-right: 1px solid #59cd90ff;

`

const Lista = styled(Card)`
    list-style: none;
    background-color: #fff;
    border-top: 2px solid #ff66d8ff;
    border-bottom: 2px solid #ff66d8ff;
    border-left: 2px solid #59cd90ff;
    border-right: 2px solid #59cd90ff;
    margin: 10px 0;

`
const SCardBody = styled(CardBody)`
    padding:0 !important;
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: center;

`

const SCardTitle = styled(CardTitle)`
    width: 100%;
    text-align: center;
    font-size: 14px;
    margin:5px 0;
    font-weight: bold;
    color: #2B3A67;

`