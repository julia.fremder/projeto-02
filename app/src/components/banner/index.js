import styled from "styled-components";
import { Container } from 'reactstrap';


const Banner = ( ) => {
  return (
    <SBanner>
      <SContainer>
      <Top>
        <p>Colônia de férias: Fedelhos Felizes</p>
      </Top>
      <TextoBanner>
        <p>Inscrição nas atividades extras</p>
        <p>As atividades extras são oficinas temáticas pensadas de forma a
          estimular as crianças com diversos métodos, tais como artesanato,
          atividade desportiva, desenvolvimento artístico dentre outros.</p>
      </TextoBanner>
      </SContainer>
    </SBanner>
  );
};

export default Banner;

const SBanner = styled.div`
  background-color: #fafafa;
`

const Top = styled.div`


  p {
    text-align: center;
    font-size: 24px;
    height:40px;
    color: #2B3A67;
    font-weight: bold;
    margin-bottom: 0;
    margin-top: 15px;
  }
 

`

const TextoBanner = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  margin: 15px 0;
  
  p{
  text-align: center;
  max-width: 1000px;
  color: #2B3A67;
  margin-bottom: 5px;
 
 }
`

const SContainer = styled(Container)`

  display: flex;
  flex-direction: column;
  align-items: center;

`