import React from 'react'
import {useState} from 'react'
import {postServiceDetalhe} from '../../services/atividade.service';
import styled from 'styled-components';
import { Button, Form, Input } from 'reactstrap';
import ReactSwal from '../../plugins';

const FormularioInscricao = ({id, update, isForm}) => {

    const [form, setForm] = useState({})

    const handleChange = (e) => {//pega o evento como parametro
        setForm({
            ...form,            
            [e.target.name]:e.target.value
        })
        // console.log(form)
    }

    const submitForm = () => {

        const newForm = {
            ...form,
            child_name: form.crianca_nome.toUpperCase(),
            parent_name: form.crianca_nome_responsavel.toUpperCase(),
            parent_email: form.crianca_email_responsavel.toLowerCase(),

        }

        postServiceDetalhe(id, newForm)
        .then(()=>{
            ReactSwal.fire({
                position: 'center',
                icon: 'success',
                title: 'Cadastro realizado.',
                showConfirmButton: false,
                timer: 1500
              })
            setForm({});//depois de realizado o cadastro é necessário esvaziar o formulário para nova utilização.
            update(true); //quando seta o update para true a funcao useEffect que está no detalhe percebe que houve alteração e chama de novo o ciclo de vida de render e completa as informações novas no getDetalhes.  
            isForm(false);
        })
        .catch(erro=>console.log('deu ruim...'))
    }


    return (
       
       <Form className="container">
        

          <SInput type="text"
                name="crianca_nome"
                value={form.crianca_nome || ""}
                onChange={handleChange}
                placeholder="Primeiro e último nome da criança"
                className="text-capitalize"
                />
          <SInput type="date"
                name="crianca_data_nascimento"
                value={form.crianca_data_nascimento || ""}
                onChange={handleChange}
                placeholder="Data de nascimento da criança"
                />
        <SInput type="text"
                name="crianca_nome_responsavel"
                value={form.crianca_nome_responsavel || ""}
                onChange={handleChange}
                placeholder="Primeiro e último nome do responsável"
                className="text-capitalize"
                />
        
        <SInput type="email"
                name="crianca_email_responsavel"
                value={form.crianca_email_responsavel || ""}
                onChange={handleChange}
                placeholder="E-mail do responsável"
                className="text-lowercase"
                />
                
                <SButton><Button color="success" onClick={submitForm}>Cadastrar</Button></SButton>
      </Form>

    )
}

export default FormularioInscricao



const SButton = styled.p`
   margin: 10px auto;
   max-width: 800px;

`

const SInput = styled(Input)`
   
    margin: 10px auto;
    max-width: 800px;
   
    

`
