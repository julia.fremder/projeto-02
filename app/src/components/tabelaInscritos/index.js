import React from 'react'
import {useParams} from 'react-router'
import { useState } from 'react';
import {deleteServiceDetalhe} from '../../services/atividade.service'
import styled from 'styled-components'
import { Table, Button, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
import { BsTrash } from "react-icons/bs";
import ReactSwal from '../../plugins';



const TabelaInscritos = ({inscritos, update}) => {

    // console.log("inscritos", inscritos)

    const {id: atividade_id} = useParams();
    
    const [modal, setModal] = useState ({
        modalShow: false,
        data: null
    })
    
    // console.log("modal data:", modal.data)

    const deleteInscricao = () => {
        if (modal.data.id){
        deleteServiceDetalhe(atividade_id, modal.data.id)
            .then(()=> {
                ReactSwal.fire({
                    icon: 'success',
                    title: `A criança ${modal?.data?.crianca_nome?.split(" ")[0]} foi descadastrada!`,
                    showConfirmButton: false,
                    showCloseButton: true,
                })
                update(true)
            })
            .catch(erro => alert('a criança não foi excluída devido a' + erro))

    }}

    const toggleModal = (data = null) => {
        // console.log("data",  data)
        setModal({
            modalShow: !modal.modalShow,
            data
            
        })
    }

    return (
        <div>
            {inscritos && inscritos.length ? (
                <>
                <STable className="container" responsive borderless  striped>
                    <thead className="bg-primary">
                        <Str>
                            <th>Nome da Criança</th>
                            <th>Nascimento da Criança</th>
                            <th>Nome do Responsável</th>
                            <th>E-mail do Responsável</th>
                            <th>Ações</th>
                        </Str>
                    </thead>
                    <tbody className= "table-primary">
                        {inscritos && inscritos.map((valor, i) => (
                            <Str key={i}>
                                <td>{valor.crianca_nome}</td>
                                <td>{new Date(valor.crianca_data_nascimento).toLocaleDateString()}</td>
                                <td>{valor.crianca_nome_responsavel}</td>
                                <td>{valor.crianca_email_responsavel}</td>
                                <td><Sbutton onClick={()=> toggleModal(valor)}><BsTrash size="20"/></Sbutton></td>

                            </Str>
                        ))}

                    </tbody>
                </STable>
                <Modal isOpen={modal.modalShow} toggle={toggleModal}>
                <ModalHeader toggle={toggleModal}>Excluir criança da atividade:</ModalHeader>
                <ModalBody>
                    Deseja descadastrar a criança <strong>{modal?.data?.crianca_nome?.split(" ")[0]}</strong>?
                </ModalBody>
                <ModalFooter>
    
                    <Button color="success" onClick={() => deleteInscricao()}>SIM</Button>
                    <Button color="danger" onClick={toggleModal}>NÃO</Button>
                </ModalFooter>
            </Modal>
            </>
            ) : (
                <STable className="container" responsive borderless  striped>
                <thead>
                    <Str>
                        <th>Matrícula</th>
                        <th>Nome da Criança</th>
                        <th>Nascimento da Criança</th>
                        <th>Nome do Responsável</th>
                        <th>E-mail do Responsável</th>
                        <th>Ações</th>
                    </Str>
                </thead>
                <tbody>
                        <Str>
                            <td>--</td>
                            <td>------</td>
                            <td>------</td>
                            <td>------</td>
                            <td>------</td>
                            <td>---</td>
                        </Str>
                        <Str>
                            <td>--</td>
                            <td>------</td>
                            <td>------</td>
                            <td>------</td>
                            <td>------</td>
                            <td>---</td>
                        </Str>
                        <Str>
                            <td>--</td>
                            <td>------</td>
                            <td>------</td>
                            <td>------</td>
                            <td>------</td>
                            <td>---</td>
                        </Str>

                </tbody>
            </STable>
            
            )}
            
        </div>
    )
}

export default TabelaInscritos


const STable = styled(Table)`
    border-radius:4px;
    /* max-width:1000px; */
    box-sizing: border-box;

`

const Str = styled.tr`

    
    th{

       font-size: 14px;

       :nth-child(5){text-align:center;}
    }

    td{
        font-size:14px;

        :nth-child(5){text-align:center;}
    }

    

`
// const Str = styled.tr`
//     border-top: 1px solid #2B3A67;
    

// `

// const Sth = styled.th`
//     border: 1px solid #2B3A67;
//     background-color: #ffee57;
//     font-weight: 300;
//     color: #2B3A67;
//     padding: 5px;

// `
// const Std = styled.td`
//     border:1px solid #2B3A67;
//     border-right:1p solid #2B3A67;
//     color: #2B3A67;
//     padding: 5px;
//     text-align: center;


// `

const Sbutton = styled.button`
    padding: 3px;
    font-size: 10px;
    color: red;
    border: 0.5px solid #bebebe;
    border-radius: 5px;


`