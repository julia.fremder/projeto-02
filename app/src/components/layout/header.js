// import {Link} from "react-router-dom";
import { useState } from 'react'
import logo from "../../assets/logo/IFF.png";
import styled from 'styled-components';
import {NavLink as RRDNavLink} from 'react-router-dom';
import { NavbarToggler, Collapse, NavItem, NavLink, Nav, Navbar, Container, NavbarBrand } from "reactstrap";

const Header = () => {


  const [collapsed, setCollapsed] = useState(false);

  const toggleNavbar = () => setCollapsed(!collapsed);

  return (
    <header>
        <SNavbar dark expand="md">
          <Container>
        <NavbarBrand>
          <img src={logo} alt="logomarca do Instituto Fedelhos Felizes" />
          <p className="IFF">Instituto Fedelhos Felizes</p>
        </NavbarBrand>

        <NavbarToggler color="dark" dark onClick={toggleNavbar} className="mr-2" />
        <Collapse isOpen={collapsed} navbar>
        <Nav className="mr-auto" navbar>
            <NavItem>
              <NavLink exact tag={RRDNavLink} activeClassName="active" to="/">Atividades</NavLink>
            </NavItem>
            <NavItem>
              <NavLink exact tag={RRDNavLink} activeClassName="active" to="/">
                Sobre
              </NavLink>
            </NavItem>
          </Nav>
        </Collapse>
        </Container>
        </SNavbar>
    </header>
  );
};

export default Header;

const SNavbar = styled(Navbar)`
  width:100%;
  height:8vh;
  border-bottom: 10px solid #fc661b;
  background-color:#dc1a0e;
  padding:0;

  .container {

      display: flex;
      flex-wrap: nowrap;

      .navbar-brand {

        display: flex;
        align-items: center;
        box-sizing: border-box;
        width: 100%;
        flex: 1;
        padding: 0;    
        p{
          margin: 0 0 0 1vw;
          font-weight: bold;
          color: #fff;
          text-shadow: 0px 0px 4px #188033;
          display: flex;
          flex-direction: column;
          align-self: center;
          justify-self: center;
        }

        img{

          height: 5vh;

        }


      }

      .navbar-collapse{
        flex:0;
        font-weight:600;
      }

  }

`


