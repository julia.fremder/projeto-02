import React from 'react'
import Header from './header';
import Footer from './footer';
import styled from 'styled-components'

const Layout = (props) => {

    return (
        <SLayout>
            <Header/>
            <Main>
                {props.children}
            </Main>
            <Footer/>
        </SLayout>
    )
}

export default Layout;


const SLayout = styled.div`
    display: flex;
    flex-direction: column;
    width: 100%;
    min-height: 100vh;


`

const Main = styled.main`
    flex: 1;
    display: flex;
    flex-direction: column;
    background-color: #eee;

`