import React from 'react'
import styled from 'styled-components'

const Footer = () => {
    return (
        <SFooter>
                <p className="container">todos os direitos reservados.</p>
                {/* <p>Intranet de uso exclusivo de colaboradores.<br/>Secretária cadastrada: {nome}.</p>
                <p>Em caso de alteração de secretária, favor preencher abaixo quem está gerindo o sistema:</p>
                <input type="text" onChange={(event)=> setNome(event.target.value)}></input> */}
            </SFooter>
    )
}

export default Footer;

const SFooter = styled.footer`

    background-color:#540073;
    height:40px;
    /* display: flex;
    flex-direction: column;
    justify-content: center; */
    border-top:5px solid #0048f1;
    width:100%;


    p{
        text-align: center;
        color: #fff;
        height:40px;
        line-height: 40px;
        font-size:14px;

    }

`

