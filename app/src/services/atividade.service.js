import http from '../config/http';

//usa a rota consumida para receber os dados de todas as atividades.
const getAllServiceAtividades = () => (http.get('/atividade'));


//usa a rota consumida + o id da atividade para receber os dados específicos de uma atividade, conforme o id.
const getServiceDetalhe = (id_ativ) => http.get(`/atividade/${id_ativ}`);


//usa o id da atividade e os dados gerados pelo preenchimento do formulário para enviar informações de inscriçõa para essa atividade específica.
const postServiceDetalhe = (id_ativ, data) => http.post(`/atividade/${id_ativ}/inscricao`, data);

//deleta uma inscricao especifica utilizando a informacao do id da inscricao e o id da atividade.
const deleteServiceDetalhe = (id_ativ, id_inscricao) => http.delete(`/atividade/${id_ativ}/inscricao/${id_inscricao}`);



export {
    getAllServiceAtividades,
    getServiceDetalhe,
    postServiceDetalhe,
    deleteServiceDetalhe
}

