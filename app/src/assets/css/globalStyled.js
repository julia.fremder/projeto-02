import { createGlobalStyle } from 'styled-components';


const GlobaStyle = createGlobalStyle`

    *{
        margin: 0;
        outline: 0;
        box-sizing: border-box;
        font-family: Arial, Helvetica, sans-serif;
        text-decoration: none;
    }

    }

    #root{
        display: flex;
        flex-direction:column;
        align-self: center;
        align-items: center;
        width:100%;
        background-color: #0048f1;
        
    }

`

export default GlobaStyle;