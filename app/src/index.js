import React from 'react';
import ReactDOM from 'react-dom';
import 'bootstrap/dist/css/bootstrap.min.css';
import GlobaStyle from './assets/css/globalStyled';




// routes
import Router from './router';

// const element = <React.Fragment><Router/></React.Fragment>;

ReactDOM.render(
    <React.Fragment>
        <GlobaStyle />
        <Router />
    </React.Fragment>,
    document.getElementById('root'))