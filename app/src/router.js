import React from "react";
import {
    BrowserRouter as BRouter,
    Switch,
    Route,
    // Redirect
} from "react-router-dom";

// layout
import Layout from './components/layout'

// views
import Atividades from './views/Atividades';
import Detalhes from './views/Detalhes';
import Error404 from './views/erros/404';


const Router = () => {

    return (
      <BRouter>
        <Layout titulo="Fedelhos Felizes">
          <Switch>
            <Route exact path="/" component={Atividades} />
            <Route exact path="/atividade/:id" component={Detalhes} />

            {/* <Route path='/error/404' component={Error404}/>                   
                   <Redirect exact from='*' to='/error/404'/>  */}

            <Route component={Error404} />
          </Switch>
        </Layout>
      </BRouter>
    );

}


export default Router