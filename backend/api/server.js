const express = require('express');
const cors = require('cors');
const router = require('./routes/router');

 

//settings
const app = express();
const port = process.env.PORT || 3001;


//midleware
app.use(express.json());
app.use(cors());

//routes
router(app)
/*
Um único módulo (camada) que trata de todas as rotas, por isso no server só chama um arquivo.
*/



app.listen(port, ()=> {

    console.log(`Server listening on port ${port}`);

})

module.exports = app;