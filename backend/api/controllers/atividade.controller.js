/*
no index que herda as configurações que estao disponiveis no meu ambiente para ligar minha aplicação e vou pedir pra subir5 o servidor configurado, o Server tem que configurar os middlewares do projeto (camada intermediaria que se encaixa no meio do ciclo de vida da request, pode ser local ou....).
Server tb registra as rotas, ele nao detem o conhecimento das rotas, so sabe que precisa importar o modulo de rotas e la vai registrar.
Arquivo central para gestão de rotas, um cara que permita manipular rotas de forma isolada, de forma a isolar a responsabilidade e não impactar quando for necessário alterar estrutura de rotas (alterar num único lugar) - facilidade na manutenção e interpretação. 
No arquivo de rotas tenho que registrar as rotas basicas e com operações de negocios, as operações que fazemos em cima do nosso conceito de negocios. osso objetivo é listar cursos e uma vez que eu vou expor essa atividade preciso registrar a rota de curso.
Rotas só do curso e depois só do aluno e depois as interelações.
Camada intermediária, ao inves de usar um conector, camada de abstração para diversas liguagens ORM (object relational mapper), fornece uma biblioteca onde conseguimos interagir com o DB sem complexidades. Ferramentas para manipulação dos dados. trabalho inicial de configuração, mas depois facilita o trabalho. O DB é uma app à parte da api. É necessário interagir com o DB.
Migration
*/
const {Atividade, inscricao} = require('../models');

// console.log(db);

const getAllAtividade = async (req, res) => {

    const result = await Atividade.findAll({})
    
    res.status(200).send(result.map((item) => {

          const {id, descricao, faixa_etaria_indicada, imagem_URL} = item;

            return {
                id,
                descricao,
                faixa_etaria_indicada,
                imagem_URL,
            }   
        }) || []);

    };


const getAtivById = async (req, res) => {
    //usa o parametro setado na rota especifica

    // const { course_id } = request.params

    // const registrations = await db.Course.findByPk(course_id, {
    //   include: { association: 'registrations' }
    // })
    try{
    const result = await Atividade.findOne({
        where: {
            id: req.params.id
        },
        include: {
          model: inscricao,
          as: 'inscricao',
        },
    });
    // console.log(result)

        res.status(200).send({
            id:result.id,
            descricao:result.descricao,
            faixa_etaria: result.faixa_etaria_indicada,
            imagem: result.imagem_URL,
            inscricao:result.inscricao
      
        });
      
      } catch (error) {
        console.log(error);

    res.status(500).send({ message: 'Internal server error!!' });
      }

    };

    /*res.status(200).send({
        id:result.id,
        name:result.name,
        coordinator: result.coordinator
    });
    */

const postInscricaoAtividade = async (req, res) => {

  try{

    //TODO: em qual atividade (id) será feita a inscrição?? pegar na rota.
    const {id} = req.params;
    // console.log('id da atividade: ', id);

    //TODO: recuperar valores de inscricao para cadastro. pegar no no body da request.
    const body = req.body;
    // console.log('body da atividade: ', body)

    //TODO: construir o model que será passado para o método create do sequelize que irá criar o insert no banco de dados e adicionar o registro. Esse model deve estar de acordo com a migration de criação da tabela inscrição.
    const model = {
      atividade_id: id, //atividade_id é o nome do id da atividade lá na tabela de inscricao, id é o nome do id da atividade na rota.
      crianca_nome: body.crianca_nome,
      crianca_data_nascimento: body.crianca_data_nascimento,
      crianca_nome_responsavel: body.crianca_nome_responsavel,
      crianca_email_responsavel: body.crianca_email_responsavel
    }

    //TODO: mandar para o banco de dados via sequelize
    await inscricao.create(model);

    res.status(200).send('inscricao')

  } catch (error) {

    res.status(500).send({message:'Não inseriu nova inscricao'})

  }

}

const deleteInscricao = async (req, res) => {

  try{

  //TODO: qual a inscricao deseja cancelar? pegar id na rota.
  const {idinscricao} = req.params;

  await inscricao.destroy({
    
    where: {
    
      id: idinscricao,
    
    }

  });

  res.status(200).send({message: 'Deletou a inscricao: ' + idinscricao})


  } catch (error) {

  res.status(500).send({message:'Não deletou a inscricao.'})

  }




}


module.exports = {
    getAllAtividade,
    getAtivById,
    postInscricaoAtividade,
    deleteInscricao
}

/*
-- método que será disparado quando a rota for invocada. Seja pelo browser ou por um cliente http, que nesse caso está a usar o postman.
-- as aplicações possuem um ciclo de vida, quando ficam latente durante um tempo o heroku derruba a aplicação.
-- na rota v1 de cursos eu espero a saída de uma lista de cursos (payload).
-- Nessa primeira saída, no primeiro endpoint, precisamos de todos os cursos.

const db = require('../../models/index')

module.exports = {
  async index(request, response) {
    const { course_id } = request.params

    const registrations = await db.Course.findByPk(course_id, {
      include: { association: 'registrations' }
    })

    return response.json(registrations)
  },

  async create(request, response) {
    const { course_id } = request.params
    const { id, name, email, birth_date } = request.body

    const registration = await db.Course.findByPk(course_id)

    if (!registration) {
      return response.status(400).json({ error: 'Registration not found' })
    }

    const registrations = await db.Registration.create({
      id,
      name,
      email,
      birth_date,
      course_id
    })

    return response.json(registrations)
  }
}






*/