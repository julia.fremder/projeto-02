

module.exports = (sequelize, DataTypes) => {
   
    // const inscricao = require('./tinscricao');
   
    const Atividade = sequelize.define(
        "Atividade", {
            id: {
                type: DataTypes.INTEGER,
                primaryKey: true,
                autoIncrement: true,
                unique: true,
                allowNull: false

            },
            descricao: DataTypes.TEXT,
            faixa_etaria_indicada: DataTypes.TEXT,
            imagem_URL:DataTypes.TEXT,
            status: DataTypes.BOOLEAN
        },
        {
        undescored: true,
        paranoid: true,
        timestamps: false,
        tableName:"tatividade"
    })
    
    Atividade.associate = function (models) {

        // we are adding the following now
        Atividade.hasMany(models.inscricao, {
          foreignKey: 'atividade_id',
          as: 'inscricao'
        });
      };

    // atividade.hasMany(inscricao);
      
    return Atividade;
}





// -- estou expondo uma função em que ela recebe um objeto sequelize e um objeto datatype, o sequelize no que diz respeito a models procura no .sequelizerc, e digo que está na raiz da pasta models.

// module.exports = (sequelize, DataTypes) => {
//   const Registration = sequelize.define('Registration', {
//     name: DataTypes.TEXT,
//     email: DataTypes.TEXT,
//     birth_date: DataTypes.DATE
//   })
 
//   return Registration
// }


// module.exports = (sequelize, DataTypes) => {
//   const Course = sequelize.define('Course', {
//     coordinator: DataTypes.TEXT,
//     name: DataTypes.TEXT,
//     status: DataTypes.BOOLEAN
//   })
 
//   }
//   return Course
// }
