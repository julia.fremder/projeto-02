

module.exports = (sequelize, DataTypes) => {
    const Inscricao = sequelize.define(
        "inscricao", {
            id: {
                type: DataTypes.INTEGER,
                primaryKey: true,
                autoIncrement: true,
                unique: true,
                allowNull: false

            },
            crianca_nome: DataTypes.TEXT,
            crianca_data_nascimento: DataTypes.DATE,
            crianca_nome_responsavel:DataTypes.TEXT,
            crianca_email_responsavel:DataTypes.TEXT,
            
        },
        {
        undescored: true,
        paranoid: true,
        timestamps: false,
        tableName:"tinscricao"
    })

    Inscricao.associate = function (models) {
        Inscricao.belongsTo(models.Atividade, {
          foreignKey: 'atividade_id',
          as: 'atividades'
        });
      };


    

    return Inscricao ;
}