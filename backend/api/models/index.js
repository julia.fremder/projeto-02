'use strict';

const fs = require('fs');
const path = require('path');
const Sequelize = require('sequelize');
const basename = path.basename(__filename);
const env = process.env.NODE_ENV || 'development';

const config = require(__dirname + '/../../db/config.js');

const db = {};

let sequelize;

if (config.use_env_variable) {

  sequelize = new Sequelize(process.env[config.use_env_variable], config);

} else {

  console.log('@AQUIIIIIIII MODELS/INDEX');

  sequelize = new Sequelize(config.database, config.username, config.password, {
    ...config,
    pool: {
      max: 2,
      min: 0,
      idle: 5000
    }
  });

}

fs
  .readdirSync(__dirname)
  .filter(file => {
    return (file.indexOf('.') !== 0) && (file !== basename) && (file.slice(-3) === '.js');
  })
  .forEach(file => {
    const model = require(path.join(__dirname, file))(sequelize, Sequelize.DataTypes);
    db[model.name] = model;
  });

Object.keys(db).forEach(modelName => {
  if (db[modelName].associate) {
    db[modelName].associate(db);
  }
});


// const sequelize = new Sequelize(config);

// const modelAtividade = require('./Atividade');
// db.Atividade = modelAtividade(sequelize, Sequelize.DataTypes);

// const modelInscricao = require('./Inscricao.js');
// db.Inscricao = modelInscricao(sequelize, Sequelize.DataTypes);

// // db.tatividade.hasMany(db.tinscricao, { as: "inscricao" });
// // db.tinscricao.belongsTo(db.tatividade, {
// //   foreignKey: "atividade_id",
// //   as: "atividade",
// // });


// fs
//   .readdirSync(__dirname)
//   .filter(file => (file.indexOf('.') !== 0) && (file !== path.basename(__filename)) && (file.slice(-3) === '.js'))
//   .forEach((file) => {
//     const model = sequelize.import(path.join(__dirname, file));
//     db[model.name] = model;
//   });

// Object.keys(db).forEach((modelName) => {
//   if (db[modelName].associate) {
//     db[modelName].associate(db);
//   }
// });

db.sequelize = sequelize;
db.Sequelize = Sequelize;

module.exports = db;



// é na camada de models que vai fazer o mapeamento relacional.
// migrations permite manipular (construir e destruir) a estrutura fixa do db, as colunas, linhas e tabelas, relacionamentos. Apresenta o script que vai construir o banco de dados. A represetanção real do banco de dados é o model.
// abordagem codigo primeiro (antes o modelo de negocios e depois o db)
// comutador de models, cada unidade de negocio vai ter uma definição do seu schema, representação do schema e dos relacionamentos.

