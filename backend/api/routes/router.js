const { Router } = require('express');
const { name, version } = require('../../package.json');
const atividadeRoutesV1 = require('./v1/atividade.router')

module.exports = (app) => {

    const router = Router();

    router.route('/').get((req, res) => {
        res.send({name, version});
    })

    atividadeRoutesV1(router);

    app.use('/v1', router);

};
/*
-- define onde o express vai pegar as informações.
-- a rota está preparada para executar uma função do controller.
-- a rota de cursos precisa devolver uma lista de cursos.
-- separacao das responsabilidades das rotas. Trata das rotas separado, fora do index. Criamos um server com toda configuração relacionada ao servidor web do express. Usamos dois middlewares, um é o parser que o express tem e o outro é a inclusão do cors, origens do projeto, origens permitidas para consumir a api. Nãop pode deixar um serviço web aberto, a não ser que isso faca parte do negocio.
-- arquivo de rota rais, faz toda a estrutura de roteamento do projeto. Nesse momento está pequena pois só temos um recurso e uma versão para exposição de rotas.

*/