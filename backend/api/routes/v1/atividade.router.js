const atividadeController = require('../../controllers/atividade.controller')


    module.exports = (router) => {

        router.route('/atividade')
        .get(
            atividadeController.getAllAtividade
            );

        router.route('/atividade/:id')
        .get(
            atividadeController.getAtivById
            );

        router.route('/atividade/:id/inscricao')
        .post(
            atividadeController.postInscricaoAtividade
        );

        router.route('/atividade/:id/inscricao/:idinscricao')
        .delete(
            atividadeController.deleteInscricao
        )


    }


/*
-- esse modulo expoe uma função, que utiliza o objeto rota (que vai receber como parametro quando for utilizado), para registrar o endereço de curso.
-- por tras da rota configurada faz a ligação do metodo que eu quero que seja executado (ações de negocio ficam no controller).

*/