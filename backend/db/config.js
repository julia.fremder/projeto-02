const node_environment = process.env.NODE_ENV || 'development'

if(node_environment === 'development') {
    require('dotenv').config();
    
}



config = {
    // database: 'db_colonia_ferias',
    database: process.env.DB_NAME,
    username: process.env.DB_USERNAME,
    // username: 'root',
    password:process.env.DB_PASSWORD,
    //password:'teste123',
    host:process.env.DB_HOST,
    //host:'localhost',
    port:process.env.DB_PORT,
    //port:'3306',
    dialect:process.env.DB_DIALECT,
    //dialect:'mysql',
    logging:false


    
    
    
    // dialect:'mysql',
    // logging:false
}
// console.log(config.database, config.username, config.password, config.host, config.port, config.dialect)

module.exports = config;