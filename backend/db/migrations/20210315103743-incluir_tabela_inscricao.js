'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {

    await queryInterface.createTable('tinscricao', {
      
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      crianca_nome: {
        allowNull: false,
        type: Sequelize.TEXT
      },
      crianca_data_nascimento: {
        allowNull:false,
        type:Sequelize.DATE
      },
      crianca_nome_responsavel: {
        allowNull:false,
        type:Sequelize.TEXT
      },
      crianca_email_responsavel: {
        allowNull:false,
        type:Sequelize.TEXT
      },
      atividade_id:{
        type: Sequelize.INTEGER,
        references: { model: {tableName:'tatividade'}, key: 'id' },
        onDelete: 'CASCADE',
      }
      // status: {
      //   allowNull: false,
      //   type:Sequelize.BOOLEAN,
      //   defaultValue: true
      // },
      
    
    });
     
  },



  down: async (queryInterface, Sequelize) => {
    return queryInterface.dropTable('tinscricao')
  }
};

/*
'use strict'

module.exports = {
  up: async (queryInterface, Sequelize) => {
    return queryInterface.createTable('registrations', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      course_id: {
        type: Sequelize.INTEGER,
        allowNull: false,
        references: { model: 'courses', key: 'id' },
        onUpdate: 'CASCADE',
        onDelete: 'CASCADE'
      },
      name: {
        allowNull: false,
        type: Sequelize.TEXT
      },
      email: {
        allowNull: false,
        type: Sequelize.TEXT
      },
      birth_date: {
        allowNull: false,
        type: Sequelize.DATE
      },
      created_at: {
        type: Sequelize.DATE,
        allowNull: false
      },
      updated_at: {
        type: Sequelize.DATE,
        allowNull: false
      }
    })
  },

  down: async (queryInterface, Sequelize) => {
    return queryInterface.dropTable('registrations')
  }
}
*/
