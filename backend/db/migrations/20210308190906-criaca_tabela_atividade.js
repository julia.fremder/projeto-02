'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
 
    await queryInterface.createTable('tatividade', {
      
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      descricao: {
        allowNull: false,
        type: Sequelize.TEXT
      },
      faixa_etaria_indicada: {
        allowNull:false,
        type:Sequelize.TEXT
      },
      imagem_URL:{
        type:Sequelize.TEXT
      },
      status: {
        allowNull: false,
        type:Sequelize.BOOLEAN,
        defaultValue: true
      },
    });
     
  },

  down: async (queryInterface, Sequelize) => {
    return queryInterface.dropTable('tatividade');
  }
};

/*
'use strict'

module.exports = {
  up: async (queryInterface, Sequelize) => {
    return queryInterface.createTable('courses', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      name: {
        allowNull: false,
        type: Sequelize.TEXT
      },
      coordinator: {
        allowNull: false,
        type: Sequelize.TEXT
      },
      status: {
        allowNull: false,
        type: Sequelize.BOOLEAN,
        defaultValue: true
      },
      created_at: {
        type: Sequelize.DATE,
        allowNull: false
      },
      updated_at: {
        type: Sequelize.DATE,
        allowNull: false
      }
    })
  },

  down: async (queryInterface, Sequelize) => {
    return queryInterface.dropTable('courses')
  }
}
*/